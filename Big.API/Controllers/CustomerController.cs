﻿using Big.DAL.Abstractions;
using Big.DAL.Abstractions.Operations;
using Big.Core.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Big.DAL.Models;
using Big.Core.Models;

namespace Big.DAL.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CustomerController: ControllerBase
    {
        private readonly IBigBL _BigBL;

        public CustomerController(IBigBL bigBL)
        {
            _BigBL = bigBL;
        }
        [HttpGet("/Customers")]
        public async Task<IActionResult> GetCustomersAsync([FromQuery] FilterCustomersView filter)
        {
             await _BigBL.GetCustomersAsync(filter);
           
            return Ok("It's very good");
        }
    }
}
