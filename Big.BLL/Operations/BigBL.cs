﻿using Big.Core.Entities;
using Big.DAL.Abstractions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Big.DAL.Abstractions.Operations;
using Big.Core.Abstractions.Repositories;
using Big.DAL.Models;
using Big.Core.Models;

namespace Big.BLL.Operations
{
    public class BigBL :IBigBL
    {
        
        private readonly IRepositoryManager _repositories;

        public BigBL(IRepositoryManager repositories)
        {
            _repositories = repositories;
        }

        public async Task<IEnumerable<Customer>> GetCustomersAsync(FilterCustomersView filter)
        {
            var customers = await _repositories.Customers.GetFilteredCustomersAsync(filter);

            return customers.Select(x => new Customer
            {
                CustomerId = x.CustomerId,
                Country = x.Country,
                City = x.City,
            });


        }
        //public async Task<IEnumerable<Customer>> GetCustomersAsync(FilterCustomersView filter, FilterOrdersView filter2)
        //{
        //    await TestTransaction(filter, filter2);

        //}
        //public async Task TestTransaction(FilterCustomersView filter, FilterOrdersView filter2)
        //{
        //    using var transaction = _repositories.BeginTransaction();
        //    try
        //    {
        //        var customers = await _repositories.Customers.GetFilteredCustomersAsync( filter);

        //         customers.Select(x => new FilterCustomersView
        //        {
        //             C_ID = x.CustomerId,
        //             Company = x.CompanyName,
        //        });

        //        var orders = await _repositories.Orders.GetFilteredOrdersAsync(filter2);

        //        orders.Select(x => new FilterOrdersView
        //        {
        //            OrderID = x.OrderId,
        //            CustomerID = x.CustomerId,
        //        }) ;

        //       /// await /////////////////harc vonc 2nel select anem?? te esi petq chi
        //     //   transaction.Commit();
        //    }
        //    catch (Exception)
        //    {
        //        transaction.Rollback();
        //        throw;
        //    }

        //}


    }
}
