﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.Core.Models
{
    public class FilterCustomersView
    {
        public string  C_ID { get; set; }
        public string Company { get; set; }
        public string City { get; set; }
    }
}
