﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.Core.Models
{
   public class FilterOrdersView
    {
        public int? OrderID { get; set; }
        public string CustomerID { get; set; }
    }
}
