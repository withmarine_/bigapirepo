﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.Core.Abstractions.Repositories
{
    public interface IRepositoryBase<T>
        where T : class
    {

        Task<IEnumerable<T>> GetAll();

        Task<T> GetAsync(int id);
        T Add(T entity);
    }
}
