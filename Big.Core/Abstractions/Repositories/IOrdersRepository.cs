﻿using Big.Core.Models;
using Big.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.Core.Abstractions.Repositories
{
    public interface IOrdersRepository : IRepositoryBase<Order>
    {
        Task<IEnumerable<Order>> GetFilteredOrdersAsync(FilterOrdersView filter);
    }
}
