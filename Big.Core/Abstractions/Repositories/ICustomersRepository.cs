﻿using Big.Core.Entities;
using Big.Core.Models;
using Big.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.Core.Abstractions.Repositories
{
   public interface ICustomersRepository: IRepositoryBase<Customer>
    {
        Task<IEnumerable<Customer>> GetFilteredCustomersAsync(FilterCustomersView filter);
    }
}
