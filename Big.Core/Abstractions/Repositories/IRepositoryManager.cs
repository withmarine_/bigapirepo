﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.Core.Abstractions.Repositories
{
    public interface IRepositoryManager
    {
        public ICustomersRepository Customers { get; }
        public IOrdersRepository Orders { get; }


        void SaveChanges();
        Task SaveChangesAsync();

        IDatabaseTransaction BeginTransaction();
    }
}
