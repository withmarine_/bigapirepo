﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.Core.Abstractions
{
   public interface IDatabaseTransaction:IDisposable
    {
        void Commit();
        void Rollback();
    }
}
