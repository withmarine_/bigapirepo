﻿using Big.Core.Entities;
using Big.Core.Models;
using Big.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.DAL.Abstractions.Operations
{
    public interface IBigBL
    {
        Task<IEnumerable<Customer>> GetCustomersAsync(FilterCustomersView filter);

    }
}
