﻿using Big.Core.Abstractions.Repositories;
using Big.Core.Models;
using Big.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.DAL.Repository
{
    public class CustomersRepository: RepositoryBase<Customer>, ICustomersRepository
    {
        public CustomersRepository(NorthwindContext context)
             : base(context)
        {

        }

        public async Task<IEnumerable<Customer>> GetFilteredCustomersAsync(FilterCustomersView filter)
        {
            var query = _context.Customers.AsQueryable();

            if (filter.C_ID != null)
            {
                query = query.Where(x => x.CustomerId == filter.C_ID);
            }


            return await query.ToArrayAsync();
        }
    }
}
