﻿using Big.Core.Abstractions.Repositories;
using Big.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.DAL.Repository
{
   public class RepositoryBase<T>: IRepositoryBase<T>
        where T : class
    {
        protected readonly NorthwindContext _context;

        public RepositoryBase(NorthwindContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<T>> GetAll()
        {
            return await _context.Set<T>().ToListAsync();
        }

        public Task<T> GetAsync(int id)
        {
            throw new NotImplementedException();
        }
        public T Add(T student)
        {
            _context.Set<T>().Add(student);
            return student;
        }



    }
}
