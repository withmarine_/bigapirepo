﻿using Big.Core.Abstractions.Repositories;
using Big.Core.Models;
using Big.DAL.Models;
using Big.DAL.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.DAL.Repositories
{
   public class OrderRepository: RepositoryBase<Order>, IOrdersRepository
    {
        public OrderRepository(NorthwindContext context)
             : base(context)
        {

        }


        public async Task<IEnumerable<Order>> GetFilteredOrdersAsync(FilterOrdersView filter)
        {
            var query = _context.Orders.AsQueryable();

            if (filter.OrderID.HasValue)
            {
                query = query.Where(x => x.OrderId == filter.OrderID);
            }

            if (!string.IsNullOrEmpty(filter.CustomerID))
            {
                query = query.Where(x => x.CustomerId.ToLower().Contains(filter.CustomerID.ToLower()));
            }

            return await query.ToArrayAsync();
        }

    }
}
