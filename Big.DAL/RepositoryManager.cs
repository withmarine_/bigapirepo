﻿using Big.Core.Abstractions;
using Big.Core.Abstractions.Repositories;
using Big.DAL.Models;
using Big.DAL.Repositories;
using Big.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.DAL
{
    public class RepositoryManager : IRepositoryManager
    {

        private readonly NorthwindContext _context;

        public RepositoryManager(NorthwindContext context)
        {
            _context = context;
        }
        private ICustomersRepository _cusomters;
        public ICustomersRepository Customers => _cusomters ??= new CustomersRepository(_context);
        private IOrdersRepository _orders;

        public IOrdersRepository Orders => _orders ??= new OrderRepository(_context);

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public Task SaveChangesAsync()
        {

            return _context.SaveChangesAsync();
        }
        public IDatabaseTransaction BeginTransaction()
        {
            return new DatabaseTransaction(_context, System.Data.IsolationLevel.ReadCommitted);
        }
    }
}
